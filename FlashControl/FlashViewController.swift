//
//  FlashViewController.swift
//  FlashControl
//
//  Created by Mackwell on 11/12/2015.
//  Copyright © 2015 Weiyuan Zhu. All rights reserved.
//

import UIKit
import AVFoundation

class FlashViewController: UIViewController {
    
    var device: AVCaptureDevice?
    var captureSession: AVCaptureSession?
    var previewLayer : AVCaptureVideoPreviewLayer?
    var count = 0
    var videoConnection: AVCaptureConnection?
    var stillImageOutput: AVCaptureStillImageOutput?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        
        if device!.torchMode == AVCaptureTorchMode.Off {
            
            do{
                try device?.lockForConfiguration()
            } catch {
                print(error)
            }
            
            device?.focusMode = .Locked
            device?.flashMode = .On
            device?.torchMode = .On
            device?.unlockForConfiguration()
        }


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func takePicture(sender: AnyObject) {
        
        count = 0
        
        var input: AVCaptureDeviceInput?
        captureSession =  AVCaptureSession()
        
        do {
            input = try AVCaptureDeviceInput(device: device)
            captureSession!.removeInput(input)
            captureSession!.addInput(input)
        } catch {
            print(error)
        }
        
        stillImageOutput = AVCaptureStillImageOutput()
        let outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
        stillImageOutput?.outputSettings = outputSettings
        
        
        
        captureSession!.addOutput(stillImageOutput)
        captureSession!.startRunning()
        
        videoConnection = stillImageOutput?.connectionWithMediaType(AVMediaTypeVideo)
        
        if videoConnection != nil {
            
//            print(NSDate().timeIntervalSince1970 * 1000)
            
            
            stillImageOutput?.captureStillImageAsynchronouslyFromConnection(videoConnection, completionHandler: capture)
                
                //                print(NSDate().timeIntervalSince1970 * 1000)
                
                
//                if imageSampleBuffer != nil {
//                    
//                    let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageSampleBuffer)
//                    let dataProvider  = CGDataProviderCreateWithCFData(imageData)
//                    let cgImageRef = CGImageCreateWithJPEGDataProvider(dataProvider, nil, true, CGColorRenderingIntent.RenderingIntentDefault)
//                    
//                    var image:UIImage!
                
                    
//                    image = UIImage(CGImage: cgImageRef!, scale: 1.0, orientation: UIImageOrientation.Right)
                
                    
                    
                    //                    self.tempImageView.image = image
                    //                    self.tempImageView.hidden = false
                    
//
            
        }
    }
    
    func capture(imageSampleBuffer: CMSampleBufferRef!, error: NSError!) {
        
        self.count += 1
        
        if self.count < 3 {
            stillImageOutput?.captureStillImageAsynchronouslyFromConnection(videoConnection, completionHandler: capture)
            
        }

        
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

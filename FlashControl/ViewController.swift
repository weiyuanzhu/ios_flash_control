//
//  ViewController.swift
//  FlashControl
//
//  Created by Weiyuan Zhu on 29/11/2015.
//  Copyright © 2015 Weiyuan Zhu. All rights reserved.
//

import UIKit
import AVFoundation
//import Foundation


extension Double {
    /// Rounds the double to decimal places value
    func roundToPlaces(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return round(self * divisor) / divisor
    }
}


class ViewController: UIViewController {
    
    
    //MARK: Properties
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var mLabel: UILabel!
    
    var timer: NSTimer?
    var timeInterval: Double = 1.0
    
    var device: AVCaptureDevice?
    var captureSession: AVCaptureSession?
    var previewLayer : AVCaptureVideoPreviewLayer?
    
    var flashIsOn = false
    

    //MARK: Life Circle
    override func viewDidLoad() {
        
        //init timer
        timer = NSTimer.scheduledTimerWithTimeInterval(timeInterval, target: self, selector: "flashTest", userInfo: nil, repeats: true)
        timer?.invalidate()
        
        device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        print("next")
       
    }
    
    
    @IBAction func flashButtonPressed(sender: UIButton) {
        
        //swap image
        if flashIsOn {
            flashIsOn = false
            flashButton.setImage(UIImage(named: "Play1Pressed"), forState: .Normal)
            timer!.invalidate()
            switchOffFlash()
            
        } else {
            flashIsOn = true
            flashButton.setImage(UIImage(named: "Stop1PressedBlue.png"), forState: .Normal)
            
            timer = NSTimer.scheduledTimerWithTimeInterval(timeInterval, target: self, selector: "toggleFlash", userInfo: nil, repeats: true)
            
        }

//        if device!.torchMode == AVCaptureTorchMode.Off {
//            
//            do{
//                try device?.lockForConfiguration()
//            } catch {
//                print(error)
//            }
//            
//            device?.focusMode = .Locked
//            device?.flashMode = .On
//            device?.torchMode = .On
//            device?.unlockForConfiguration()
    
            
            
//            previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
//            self.view.layer.addSublayer(previewLayer!)
//            previewLayer?.frame = self.view.layer.frame
//            captureSession.startRunning()

        
    }
    
        
    
    @IBAction func sliderValueChanged(sender: UISlider) {
        let currentValue = Int(slider.value)
        mLabel.text = "\(currentValue) Hz"
        let tmp =  Double(1) / Double(currentValue * 2)
        timeInterval = tmp.roundToPlaces(3)
        
        
    }
    
    
    func toggleFlash() {
        
        print(NSDate().timeIntervalSince1970 * 1000)

        if device!.hasFlash && device!.hasTorch {
            do {
                try device?.lockForConfiguration()
                if device!.torchMode == AVCaptureTorchMode.On {
                    device!.torchMode = AVCaptureTorchMode.Off
                    device!.flashMode = AVCaptureFlashMode.On
                } else {
                    device!.torchMode = AVCaptureTorchMode.On
                    device!.flashMode = AVCaptureFlashMode.On
                    try device!.setTorchModeOnWithLevel(1.0)
                    
                }
                
                device?.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
    }
    
    func switchOffFlash() {
        if device!.hasTorch {
            do {
                try device?.lockForConfiguration()
                
                device?.torchMode = AVCaptureTorchMode.Off
                
                
                device?.unlockForConfiguration()
            } catch {
                print(error)
            }
        }

        
    }
    @IBAction func sliderEditingDidEnd(sender: UISlider) {
        if timer!.valid {
            timer!.invalidate()
            timer = NSTimer.scheduledTimerWithTimeInterval(timeInterval, target: self, selector: "flashTest", userInfo: nil, repeats: true)
        }

    }
    

    @IBAction func touchUpInside(sender: UISlider) {
        if timer!.valid {
            timer!.invalidate()
            timer = NSTimer.scheduledTimerWithTimeInterval(timeInterval, target: self, selector: "toggleFlash", userInfo: nil, repeats: true)
            
        } else {
            timer = NSTimer.scheduledTimerWithTimeInterval(timeInterval, target: self, selector: "toggleFlash", userInfo: nil, repeats: true)
            
            flashButton.setImage(UIImage(named: "Stop1PressedBlue.png"), forState: .Normal)
        }
        
        flashIsOn  = true

    }
    
    
    
}



